var night_stl =    [
                     {
                       "elementType": "labels.icon",
                       "stylers": [
                         {
                           "visibility": "off"
                         }
                       ]
                     },
                     {
                       "elementType": "labels.text.fill",
                       "stylers": [
                         {
                           "color": "#000000"
                         },
                         {
                           "saturation": 36
                         },
                         {
                           "lightness": 40
                         }
                       ]
                     },
                     {
                       "elementType": "labels.text.stroke",
                       "stylers": [
                         {
                           "color": "#000000"
                         },
                         {
                           "lightness": 16
                         },
                         {
                           "visibility": "on"
                         }
                       ]
                     },
                     {
                       "featureType": "administrative",
                       "elementType": "labels",
                       "stylers": [
                         {
                           "visibility": "off"
                         }
                       ]
                     },
                     {
                       "featureType": "administrative.country",
                       "elementType": "geometry.stroke",
                       "stylers": [
                         {
                           "visibility": "on"
                         }
                       ]
                     },
                     {
                       "featureType": "administrative.land_parcel",
                       "elementType": "geometry.stroke",
                       "stylers": [
                         {
                           "visibility": "off"
                         }
                       ]
                     },
                     {
                       "featureType": "administrative.locality",
                       "elementType": "labels",
                       "stylers": [
                         {
                           "color": "#c19eb1"
                         },
                         {
                           "visibility": "simplified"
                         }
                       ]
                     },
                     {
                       "featureType": "administrative.neighborhood",
                       "elementType": "labels",
                       "stylers": [
                         {
                           "visibility": "off"
                         }
                       ]
                     },
                     {
                       "featureType": "administrative.province",
                       "elementType": "geometry.stroke",
                       "stylers": [
                         {
                           "visibility": "off"
                         }
                       ]
                     },
                     {
                       "featureType": "landscape",
                       "elementType": "geometry",
                       "stylers": [
                         {
                           "color": "#000000"
                         },
                         {
                           "lightness": 20
                         }
                       ]
                     },
                     {
                       "featureType": "landscape",
                       "elementType": "geometry.fill",
                       "stylers": [
                         {
                           "color": "#2c001e"
                         }
                       ]
                     },
                     {
                       "featureType": "poi",
                       "elementType": "geometry",
                       "stylers": [
                         {
                           "lightness": 21
                         }
                       ]
                     },
                     {
                       "featureType": "poi",
                       "elementType": "geometry.fill",
                       "stylers": [
                         {
                           "visibility": "off"
                         }
                       ]
                     },
                     {
                       "featureType": "poi",
                       "elementType": "geometry.stroke",
                       "stylers": [
                         {
                           "visibility": "off"
                         }
                       ]
                     },
                     {
                       "featureType": "poi.business",
                       "stylers": [
                         {
                           "visibility": "off"
                         }
                       ]
                     },
                     {
                       "featureType": "poi.park",
                       "elementType": "geometry.fill",
                       "stylers": [
                         {
                           "color": "#210017"
                         },
                         {
                           "visibility": "on"
                         }
                       ]
                     },
                     {
                       "featureType": "poi.park",
                       "elementType": "labels.text",
                       "stylers": [
                         {
                           "visibility": "off"
                         }
                       ]
                     },
                     {
                       "featureType": "road",
                       "elementType": "labels.text",
                       "stylers": [
                         {
                           "color": "#b575af"
                         },
                         {
                           "visibility": "simplified"
                         }
                       ]
                     },
                     {
                       "featureType": "road.arterial",
                       "elementType": "geometry",
                       "stylers": [
                         {
                           "color": "#000000"
                         },
                         {
                           "lightness": 18
                         }
                       ]
                     },
                     {
                       "featureType": "road.arterial",
                       "elementType": "geometry.fill",
                       "stylers": [
                         {
                           "color": "#56334b"
                         }
                       ]
                     },
                     {
                       "featureType": "road.highway",
                       "elementType": "geometry.fill",
                       "stylers": [
                         {
                           "color": "#ed764d"
                         },
                         {
                           "lightness": 17
                         }
                       ]
                     },
                     {
                       "featureType": "road.highway",
                       "elementType": "geometry.stroke",
                       "stylers": [
                         {
                           "color": "#000000"
                         },
                         {
                           "lightness": 29
                         },
                         {
                           "weight": 0.2
                         }
                       ]
                     },
                     {
                       "featureType": "road.highway",
                       "elementType": "labels.text",
                       "stylers": [
                         {
                           "color": "#090909"
                         },
                         {
                           "visibility": "simplified"
                         }
                       ]
                     },
                     {
                       "featureType": "road.highway.controlled_access",
                       "elementType": "geometry.fill",
                       "stylers": [
                         {
                           "color": "#e95420"
                         }
                       ]
                     },
                     {
                       "featureType": "road.local",
                       "elementType": "geometry",
                       "stylers": [
                         {
                           "color": "#000000"
                         },
                         {
                           "lightness": 16
                         }
                       ]
                     },
                     {
                       "featureType": "road.local",
                       "elementType": "geometry.fill",
                       "stylers": [
                         {
                           "color": "#411934"
                         }
                       ]
                     },
                     {
                       "featureType": "road.local",
                       "elementType": "labels.text",
                       "stylers": [
                         {
                           "visibility": "off"
                         }
                       ]
                     },
                     {
                       "featureType": "transit",
                       "elementType": "geometry",
                       "stylers": [
                         {
                           "color": "#000000"
                         },
                         {
                           "lightness": 19
                         }
                       ]
                     },
                     {
                       "featureType": "transit",
                       "elementType": "geometry.fill",
                       "stylers": [
                         {
                           "color": "#0b0008"
                         }
                       ]
                     },
                     {
                       "featureType": "transit.station.airport",
                       "elementType": "geometry.fill",
                       "stylers": [
                         {
                           "color": "#1d0014"
                         }
                       ]
                     },
                     {
                       "featureType": "water",
                       "elementType": "geometry",
                       "stylers": [
                         {
                           "color": "#0d161a"
                         },
                         {
                           "lightness": 17
                         }
                       ]
                     }
                   ]
