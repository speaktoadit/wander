var day_stl = [
                {
                  "featureType": "administrative",
                  "elementType": "labels",
                  "stylers": [
                    {
                      "visibility": "off"
                    }
                  ]
                },
                {
                  "featureType": "administrative",
                  "elementType": "labels.text.fill",
                  "stylers": [
                    {
                      "color": "#444444"
                    }
                  ]
                },
                {
                  "featureType": "administrative.country",
                  "elementType": "geometry.stroke",
                  "stylers": [
                    {
                      "color": "#190217"
                    },
                    {
                      "visibility": "on"
                    }
                  ]
                },
                {
                  "featureType": "administrative.land_parcel",
                  "elementType": "geometry.stroke",
                  "stylers": [
                    {
                      "visibility": "off"
                    }
                  ]
                },
                {
                  "featureType": "administrative.locality",
                  "elementType": "labels",
                  "stylers": [
                    {
                      "color": "#902500"
                    },
                    {
                      "visibility": "simplified"
                    }
                  ]
                },
                {
                  "featureType": "administrative.neighborhood",
                  "elementType": "labels",
                  "stylers": [
                    {
                      "visibility": "off"
                    }
                  ]
                },
                {
                  "featureType": "administrative.province",
                  "elementType": "geometry.stroke",
                  "stylers": [
                    {
                      "color": "#b86eb0"
                    },
                    {
                      "visibility": "on"
                    }
                  ]
                },
                {
                  "featureType": "landscape",
                  "elementType": "geometry.fill",
                  "stylers": [
                    {
                      "color": "#e89880"
                    },
                    {
                      "lightness": 20
                    }
                  ]
                },
                {
                  "featureType": "poi",
                  "stylers": [
                    {
                      "visibility": "off"
                    }
                  ]
                },
                {
                  "featureType": "poi.business",
                  "stylers": [
                    {
                      "visibility": "off"
                    }
                  ]
                },
                {
                  "featureType": "poi.park",
                  "elementType": "geometry.fill",
                  "stylers": [
                    {
                      "color": "#da7e63"
                    },
                    {
                      "lightness": 20
                    },
                    {
                      "visibility": "on"
                    }
                  ]
                },
                {
                  "featureType": "poi.park",
                  "elementType": "labels.text",
                  "stylers": [
                    {
                      "visibility": "off"
                    }
                  ]
                },
                {
                  "featureType": "road",
                  "stylers": [
                    {
                      "saturation": -100
                    },
                    {
                      "lightness": 45
                    }
                  ]
                },
                {
                  "featureType": "road.arterial",
                  "elementType": "geometry.fill",
                  "stylers": [
                    {
                      "color": "#ac55a6"
                    }
                  ]
                },
                {
                  "featureType": "road.arterial",
                  "elementType": "labels.icon",
                  "stylers": [
                    {
                      "visibility": "off"
                    }
                  ]
                },
                {
                  "featureType": "road.arterial",
                  "elementType": "labels.text.fill",
                  "stylers": [
                    {
                      "color": "#ffffff"
                    },
                    {
                      "visibility": "off"
                    },
                    {
                      "weight": 0.5
                    }
                  ]
                },
                {
                  "featureType": "road.arterial",
                  "elementType": "labels.text.stroke",
                  "stylers": [
                    {
                      "color": "#ffffff"
                    },
                    {
                      "weight": 4.5
                    }
                  ]
                },
                {
                  "featureType": "road.highway",
                  "stylers": [
                    {
                      "visibility": "simplified"
                    }
                  ]
                },
                {
                  "featureType": "road.highway",
                  "elementType": "geometry.fill",
                  "stylers": [
                    {
                      "color": "#9e3994"
                    }
                  ]
                },
                {
                  "featureType": "road.highway",
                  "elementType": "labels.text.fill",
                  "stylers": [
                    {
                      "color": "#ffffff"
                    },
                    {
                      "visibility": "simplified"
                    }
                  ]
                },
                {
                  "featureType": "road.highway.controlled_access",
                  "elementType": "geometry.fill",
                  "stylers": [
                    {
                      "color": "#6c2265"
                    }
                  ]
                },
                {
                  "featureType": "road.highway.controlled_access",
                  "elementType": "labels.text.fill",
                  "stylers": [
                    {
                      "color": "#ffffff"
                    },
                    {
                      "visibility": "simplified"
                    }
                  ]
                },
                {
                  "featureType": "road.local",
                  "elementType": "geometry.fill",
                  "stylers": [
                    {
                      "color": "#b86eb0"
                    }
                  ]
                },
                {
                  "featureType": "road.local",
                  "elementType": "labels",
                  "stylers": [
                    {
                      "visibility": "off"
                    }
                  ]
                },
                {
                  "featureType": "road.local",
                  "elementType": "labels.text.fill",
                  "stylers": [
                    {
                      "color": "#ed3d00"
                    },
                    {
                      "visibility": "simplified"
                    }
                  ]
                },
                {
                  "featureType": "road.local",
                  "elementType": "labels.text.stroke",
                  "stylers": [
                    {
                      "color": "#ed3d00"
                    }
                  ]
                },
                {
                  "featureType": "transit",
                  "elementType": "geometry.fill",
                  "stylers": [
                    {
                      "color": "#C6C1BB"
                    }
                  ]
                },
                {
                  "featureType": "transit.station.airport",
                  "elementType": "geometry.fill",
                  "stylers": [
                    {
                      "color": "#c96a4e"
                    },
                    {
                      "lightness": 20
                    }
                  ]
                },
                {
                  "featureType": "water",
                  "stylers": [
                    {
                      "color": "#46bcec"
                    },
                    {
                      "visibility": "on"
                    }
                  ]
                },
                {
                  "featureType": "water",
                  "elementType": "geometry.fill",
                  "stylers": [
                    {
                      "color": "#7ea1b1"
                    }
                  ]
                },
                {
                  "featureType": "water",
                  "elementType": "labels.text.fill",
                  "stylers": [
                    {
                      "color": "#ffffff"
                    }
                  ]
                }
              ]