var dateObj = new Date();
var hours = dateObj.getHours();

var stl;

if(hours > 7 && hours < 20) {
    stl = day_stl;
}
else {
    stl = night_stl;
}

var map;
var Reston_VA = {
    lat: 38.958,
    lng: -77.357
};

function initMap() {
    var marker_json = $.ajax({url: '/markers.json', async: false});

    var map_options = {
        disableDefaultUI: true,
        disableDoubleClickZoom: true,
        center: Reston_VA,
        zoom: 12,
        mapTypeId: 'roadmap',
        styles: stl
    };

    map = new google.maps.Map(document.getElementById('map'), map_options);

    var nature_marker = {
        url: '/images/forest.png',
        size: new google.maps.Size(32, 37),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 32)
    };
    var history_marker = {
        url: '/images/castle.png',
        size: new google.maps.Size(32, 37),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 32)
    };
    var shops_marker = {
        url: '/images/supermarket.png',
        size: new google.maps.Size(32, 37),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 32)
    };
    var new_marker = {
        url: '/images/symbol.png',
        size: new google.maps.Size(32, 37),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 32)
    };


    var last_opened = null;
    function set_marker(marker) {
        var coordinates = {
            lat: parseFloat(marker.lat),
            lng: parseFloat(marker.lon)
        };

        var icon = null;

        switch (marker.kind) {
            case "nature":
                icon = nature_marker;
                break;
            case "history":
                icon = history_marker;
                break;
            case "shops":
                icon = shops_marker;
                break;
        }

        var new_marker = new google.maps.Marker({
            position: coordinates,
            title: marker.title,
            map: map,
            icon: icon
        });

        var contentString =
                    '<div id="content">' +
                    '<div id="siteNotice">' +
                    '</div>' +
                    '<h1 id="firstHeading" class="firstHeading">' + marker.title + '</h1>' +
                    '<div id="bodyContent">' +
                    '"<img src="' + marker.medium + '"/>' +
                    '<p>' + marker.description + '</p>' +
                    '<p><a href=" ' + marker.link + ' ">' +
                    'More Info</a>' +
                    '.</p>' +
                    '</div>' +
                    '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString
        });

        google.maps.event.addListener(new_marker, 'click', function() {
              if(last_opened != null) {
                  last_opened.close();
              }
              last_opened = infowindow;
              infowindow.open(map,new_marker);
        });

        // Event that closes the Info Window with a click on the map
        google.maps.event.addListener(map, 'click', function() {
             infowindow.close();
        });

    }

    function populate_map(marker_infos) {
        for(i = 0; i < marker_infos.length; i++) {
            set_marker(marker_infos[i]);
        }
    }
    populate_map(marker_json.responseJSON);

    var last_opened_form = null;
    google.maps.event.addListener(map, 'dblclick', function(event) {
       askMarkerType(event.latLng);
    });

    function askMarkerType(location) {
        var form_marker = new google.maps.Marker({
            icon: new_marker,
            position: location,
            map: map
        });

        var x = document.getElementById("form");
        x.style.display = 'block';

        var form_window = new google.maps.InfoWindow({
          content: x
        });

        if(last_opened_form != null) {
           last_opened_form.close();
        }
        form_marker.addListener('click', function() {
            if(last_opened_form != null) {
               last_opened_form.close();
            }
            last_opened_form = form_window;
            form_window.open(map, form_marker);
        });

        last_opened_form = form_window;

        form_window.open(map, form_marker);

        globallocation = location;

    };
}

var title;
var description;
var link;
var kind;
var globallocation;

function saveData() {
    title = escape(document.getElementById('title').value);
    description = escape(document.getElementById('description').value);
    link = escape(document.getElementById('link').value);
    kind = escape(document.getElementById('kind').value);

    PlaceNewMarker(location, map);
};

function PlaceNewMarker(form_marker, map) {
    $.ajax({
        type: "POST",
        url: "/markers",
        data: {marker: {title: title, lat: globallocation.lat(), lon: globallocation.lng() ,kind: kind, description: description, link: link}},
        success: function(data){}
    });
    document.location.reload();
}
