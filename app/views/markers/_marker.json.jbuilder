json.extract! marker, :id, :title, :lat, :lon, :kind, :description, :link, :icon, :created_at, :updated_at
json.url marker_url(marker, format: :json)
json.thumb marker.icon.url(:thumb)
json.medium marker.icon.url(:medium)