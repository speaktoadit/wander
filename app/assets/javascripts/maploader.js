var dateObj = new Date();
var hours = dateObj.getHours();

var stl;

if(hours > 7 && hours < 20) {
    stl = day_stl;
}
else {
    stl = night_stl;
}

var map;
var Reston_VA = {
    lat: 38.958,
    lng: -77.357
};

function initMap() {
    var marker_json = $.ajax({url: '/markers.json', async: false});

    var map_options = {
        disableDefaultUI: true,
        disableDoubleClickZoom: true,
        center: Reston_VA,
        zoom: 12,
        mapTypeId: 'roadmap',
        styles: stl
    };

    map = new google.maps.Map(document.getElementById('map'), map_options);

    var nature_marker = {
        url: '/images/forest.png',
        size: new google.maps.Size(32, 37),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 32)
    };
    var history_marker = {
        url: '/images/castle.png',
        size: new google.maps.Size(32, 37),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 32)
    };
    var shops_marker = {
        url: '/images/supermarket.png',
        size: new google.maps.Size(32, 37),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 32)
    };
    var new_marker = {
        url: '/images/symbol.png',
        size: new google.maps.Size(32, 37),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, 32)
    };


    var last_opened = null;
    function set_marker(marker) {
        var coordinates = {
            lat: parseFloat(marker.lat),
            lng: parseFloat(marker.lon)
        };

        var icon = null;

        switch (marker.kind) {
            case "nature":
                icon = nature_marker;
                break;
            case "history":
                icon = history_marker;
                break;
            case "shops":
                icon = shops_marker;
                break;
        }

        var new_marker = new google.maps.Marker({
            position: coordinates,
            title: marker.title,
            map: map,
            icon: icon
        });

        var contentString =
        '<div id="iw-container">' +
            '<div class="iw-title">' + marker.title + '</div>' +
            '<div class="iw-content">' +
              '<div class="iw-subTitle">Description</div>' +
              '<img src="' + marker.medium + '" alt="Porcelain Factory of Vista Alegre">' +
              '<p>' + marker.description + '</p>' +
              '<div class="iw-subTitle">More Info</div>' +
              '<p><a href=" http://' +
               marker.link +
                ' "> ' +
               'Link</a></p>'+
            '</div>' +
            '<div class="iw-bottom-gradient"></div>' +
          '</div>';

        var infowindow = new google.maps.InfoWindow({
            content: contentString,
            maxWidth: 350
        });

        google.maps.event.addListener(new_marker, 'click', function() {
              if(last_opened != null) {
                  last_opened.close();
              }
              last_opened = infowindow;
              infowindow.open(map,new_marker);
        });

        // Event that closes the Info Window with a click on the map
        google.maps.event.addListener(map, 'click', function() {
             infowindow.close();
        });

        google.maps.event.addListener(infowindow, 'domready', function() {

                // Reference to the DIV that wraps the bottom of infowindow
                var iwOuter = $('.gm-style-iw');

                /* Since this div is in a position prior to .gm-div style-iw.
                 * We use jQuery and create a iwBackground variable,
                 * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
                */
                var iwBackground = iwOuter.prev();

                // Removes background shadow DIV
                iwBackground.children(':nth-child(2)').css({'display' : 'none'});

                // Removes white background DIV
                iwBackground.children(':nth-child(4)').css({'display' : 'none'});

                // Moves the infowindow 115px to the right.
                iwOuter.parent().parent().css({left: '115px'});

                // Moves the shadow of the arrow 76px to the left margin.
                iwBackground.children(':nth-child(1)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

                // Moves the arrow 76px to the left margin.
                iwBackground.children(':nth-child(3)').attr('style', function(i,s){ return s + 'left: 76px !important;'});

                // Changes the desired tail shadow color.
                iwBackground.children(':nth-child(3)').find('div').children().css({'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index' : '1'});

                // Reference to the div that groups the close button elements.
                var iwCloseBtn = iwOuter.next();

                // Apply the desired effect to the close button
                iwCloseBtn.css({opacity: '1', right: '38px', top: '3px', border: '7px solid #48b5e9', 'border-radius': '13px', 'box-shadow': '0 0 5px #3990B9'});

                // If the content of infowindow not exceed the set maximum height, then the gradient is removed.
                if($('.iw-content').height() < 140){
                  $('.iw-bottom-gradient').css({display: 'none'});
                }

                // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
                iwCloseBtn.mouseout(function(){
                  $(this).css({opacity: '1'});
                });
              });
    }

    function populate_map(marker_infos) {
        for(i = 0; i < marker_infos.length; i++) {
            set_marker(marker_infos[i]);
        }
    }
    populate_map(marker_json.responseJSON);

    var last_opened_form = null;
    google.maps.event.addListener(map, 'dblclick', function(event) {
       askMarkerType(event.latLng);
    });

    function askMarkerType(location) {
        var form_marker = new google.maps.Marker({
            icon: new_marker,
            position: location,
            map: map
        });

        var x = document.getElementById("form");
        x.style.display = 'block';

        var form_window = new google.maps.InfoWindow({
          content: x
        });

        if(last_opened_form != null) {
           last_opened_form.close();
        }
        form_marker.addListener('click', function() {
            if(last_opened_form != null) {
               last_opened_form.close();
            }
            last_opened_form = form_window;
            form_window.open(map, form_marker);
        });

        last_opened_form = form_window;

        form_window.open(map, form_marker);

        globallocation = location;
    };
}

var title;
var description;
var link;
var kind;
var globallocation;

function saveData() {
    title = escape(document.getElementById('title').value);
    description = escape(document.getElementById('description').value);
    link = escape(document.getElementById('link').value);
    kind = escape(document.getElementById('kind').value);

    PlaceNewMarker(location, map);
};

function PlaceNewMarker(form_marker, map) {
    $.ajax({
        type: "POST",
        url: "/markers",
        data: {marker: {title: title, lat: globallocation.lat(), lon: globallocation.lng() ,kind: kind, description: description, link: link}},
        success: function(data){}
    });
    document.location.reload();
}
