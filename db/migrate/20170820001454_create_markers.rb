class CreateMarkers < ActiveRecord::Migration[5.1]
  def change
    create_table :markers do |t|
      t.string :title
      t.string :lat
      t.string :lon
      t.string :kind
      t.text :description
      t.string :link
      t.attachment :icon
      t.timestamps
    end
  end
end
