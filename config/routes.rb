Rails.application.routes.draw do

  resources :abouts


  root to: 'home#index'

  resources :markers
  get '/markers/create'



  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
